"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from index import views
from authorization import urls as auth_urls
from daily_journal_app import urls as daily_urls
import settings

urlpatterns = [

    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^user_auth/', include(auth_urls)),
    url(r'^api/v1/daily_journal/', include(daily_urls)),
    url(r'^docs/', include('rest_framework_swagger.urls')),
    url(r'^welcome_screen/$', views.WelcomeScreenView.as_view(), name='welcome_screen'),
    url(r'^media/(.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    url(r'^.*$', views.IndexView.as_view(), name='index'),
]
