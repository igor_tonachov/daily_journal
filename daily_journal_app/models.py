from django.db import models


class Notebook(models.Model):
    author = models.ForeignKey('auth.User', related_name='notebooks')
    name = models.CharField(max_length=200)

    def __unicod__(self):
        return '{0}'.format(self.name)


class Post(models.Model):
    author = models.ForeignKey('auth.User', related_name='posts')
    notebook = models.ForeignKey(Notebook, related_name='posts', blank=True, null=True)
    date_time = models.DateTimeField(auto_now_add=True)
    text = models.TextField()


class Tag(models.Model):
    name = models.CharField(max_length=200)
    author = models.ForeignKey('auth.User', related_name='tags')
    posts = models.ManyToManyField(Post, blank=True, null=True, related_name='tags')

    def __unicod__(self):
        return '{0}'.format(self.name)


class File(models.Model):
    post = models.ForeignKey(Post, related_name='files')
    file = models.FileField(upload_to='uploads/')
