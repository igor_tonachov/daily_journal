from rest_framework import filters
import django_filters
from models import Post


class CustomFilterBackend(filters.BaseFilterBackend):

    def filter_queryset(self, request, queryset, view):
        queryset = queryset.filter(author=request.user)
        return queryset


class DateFilter(filters.FilterSet):
    date_range = django_filters.DateFromToRangeFilter(name='date_time')

    class Meta:
        model = Post
        fields = ['notebook', 'tags', 'date_range']
