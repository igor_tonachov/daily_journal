from __future__ import unicode_literals

from django.apps import AppConfig


class DailyJournalAppConfig(AppConfig):
    name = 'daily_journal_app'
