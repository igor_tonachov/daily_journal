from rest_framework.generics import RetrieveUpdateDestroyAPIView, ListCreateAPIView, DestroyAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from models import Post, Notebook, Tag, File
from serializers import PostSerializer, NotebookSerializer, TagSerializer, FileSerializer
from filters import DateFilter
from rest_framework import filters


class PostListCreateAPIView(ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    filter_class = DateFilter
    search_fields = ('text',)
    # permission_classes = (IsAdminUser,)


class PostDetailUpdateDeleteAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()


class NotebookListCreateAPIView(ListCreateAPIView):
    serializer_class = NotebookSerializer
    queryset = Notebook.objects.all()


class NotebookDetailUpdateDeleteAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = NotebookSerializer
    queryset = Notebook.objects.all()


class TagListCreateAPIView(ListCreateAPIView):
    serializer_class = TagSerializer
    queryset = Tag.objects.all()


class TagDetailUpdateDeleteAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = TagSerializer
    queryset = Tag.objects.all()


class DateRangeView(APIView):

    def get(self, request, format=None):
        qs = Post.objects.filter(author=request.user)
        first_date = int(qs[0].date_time.year)
        last_date = int(qs[len(qs) - 1].date_time.year)
        years = range(first_date, last_date + 1)
        dates = []
        for year in years:
            dates.append({'value': str(year)})
            for i in qs:
                value = {'value': '{0}-{1}-01'.format(year, '%02d' % i.date_time.month)}
                if i.date_time.year == year and value not in dates:
                    dates.append(value)
        dates.sort()
        return Response(dates)


class FileView(ListCreateAPIView):
    serializer_class = FileSerializer
    queryset = File.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)


class DeleteFileView(DestroyAPIView):
    serializer_class = FileSerializer
    queryset = File.objects.all()
    filter_backends = (filters.DjangoFilterBackend,)





















