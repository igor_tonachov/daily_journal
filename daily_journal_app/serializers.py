from rest_framework import serializers
from models import Post, Notebook, Tag, File
from django.contrib.auth.models import User
# from rest_framework.fields import CurrentUserDefault


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('pk', 'username',)


class TagSerializer(serializers.ModelSerializer):
    author = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Tag
        fields = ('pk', 'name', 'author')


class NotebookSerializer(serializers.ModelSerializer):
    author = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Notebook
        fields = ('pk', 'name', 'author')


class FileSerializer(serializers.ModelSerializer):

    class Meta:
        model = File
        fields = ('pk', 'file', 'post')


class PostSerializer(serializers.ModelSerializer):
    author_data = UserSerializer(read_only=True, source='author')
    author = serializers.HiddenField(default=serializers.CurrentUserDefault())
    notebook_data = NotebookSerializer(read_only=True, source='notebook')
    tags_data = TagSerializer(read_only=True, many=True, source='tags')
    files = FileSerializer(read_only=True, many=True)

    class Meta:
        model = Post
        fields = ('pk', 'author', 'author_data', 'date_time', 'text', 'notebook', 'notebook_data', 'tags', 'tags_data', 'files')
        extra_kwargs = {'notebook': {'write_only': True}, 'tags': {'write_only': True, 'required': False}}
































