from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^posts/$', views.PostListCreateAPIView.as_view(), name='posts'),  # get all user posts or create new post
    url(r'^posts/(?P<pk>\d+)/$', views.PostDetailUpdateDeleteAPIView.as_view(), name='post_detail'),
    url(r'^notebooks/$', views.NotebookListCreateAPIView.as_view(), name='notebooks'),  # get all user notebooks or create new
    url(r'^notebooks/(?P<pk>\d+)/$', views.NotebookDetailUpdateDeleteAPIView.as_view(), name='notebook_detail'),
    url(r'^tags/$', views.TagListCreateAPIView.as_view(), name='tags'),
    url(r'^tags/(?P<pk>\d+)/$', views.TagDetailUpdateDeleteAPIView.as_view(), name='tag_detail'),
    url(r'^files/$', views.FileView.as_view(), name='files'),
    url(r'^files/(?P<pk>\d+)/$', views.DeleteFileView.as_view(), name='delete_file'),
    url(r'^dates/$', views.DateRangeView.as_view(), name='dates'),
]

