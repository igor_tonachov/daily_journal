from rest_framework import generics


class AddRequestUserAsAuthorMixin(object):

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)
