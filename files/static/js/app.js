angular.module('dailyJournal', [
    'ngRoute', 
    'tasks.module', 
    'categories.module',
    'users.module'
    ])
.config(function($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl : 'static/js/tasks/date-range.html',
                // controller  : 'TasksController'
            })
            .when('/categories', {
                templateUrl : 'static/js/categories/tasks-category.html',
                // controller  : 'TasksController'
            })
            .when('/tags', {
                templateUrl : 'static/js/tags/tags.html',
                // controller  : 'TasksController'
            });
          })
// config(['$routeProvider',function($routeProvider) {
//   $routeProvider
//    .when('/', {
//     templateUrl: 'tasks.html',
//     controller: 'TasksController'
//   })
//   .otherwise({redirectTo:'/'}); 
//   }]);
