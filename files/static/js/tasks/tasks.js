angular
  .module('tasks.module', ['auth.service', 'task.service', 'ui.bootstrap'])
  .controller('TasksController',['$scope', 'authService', 'taskService', '$uibModal', '$cookies', function($scope, authService, taskService, $uibModal, $cookies) {
    
    console.log(2);
	// public methods
  $scope.hello = "Hello World";
  $scope.registrateUser = registrateUser;
  $scope.authenticateUser = authenticateUser;
  $scope.getAllTasks = getAllTasks;
  // properties
  // $scope.isRegistating = 'false';
  // $scope.isAuthenticating = 'false';

  // definition
	function registrateUser(user) {
    authService.registrateUser(user).then(function(response) {
      $scope.data = response.data;
    })
  }

  function authenticateUser(user) {
    authService.authenticateUser(user).then(function(response) {
      $scope.authUser = response.data;
    })
  }

  function getAllTasks() {
    taskService.getAllTasks().then(function(response) {
      $scope.tasks = response.data;
      console.log($scope.tasks);
    })
  }

  $scope.open = open;
  function open() {
    var modalInstance = $uibModal.open({
      animation: $scope.animationsEnabled,
      templateUrl:'static/templates/modalForm.html',
      controller: 'ModalFormController'
      
    });
  }


  function initApp() {
    getAllTasks();
  }

  initApp();

}]);