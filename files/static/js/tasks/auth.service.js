
  'use strict';
  angular
    .module('auth.service', ['ngCookies'])
    .factory('authService', authService);
  authService.$inject = ['$http', '$filter', '$cookies', '$timeout'];
  function authService($http, $filter, $cookies, $timeout) {
    return {
     registrateUser: registrateUser,
     authenticateUser: authenticateUser
    };

    function registrateUser(user_data) {
     return $http.post('/user_auth/API_sign_up/', user_data);
  }

  function authenticateUser(user_auth_data) {
    return $http.post('/user_auth/API_login/', user_auth_data).then(function(res){
      window.setTimeout(function() {
        console.log(
        $cookies.getAll());  
      }, 1000)
      
      console.log(res);
      return res;
    });
    
  }
  }

   // $scope.changeTab = function(tabName){
   //        $scope.lastVal = tabName;
   //        $cookieStore.put('tab', tabName);
   //    };

  


