from django.views.generic.edit import FormView
from rest_framework import generics
from django.contrib.auth import login, logout
from django.shortcuts import redirect
from django.views.generic import View
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from rest_framework.views import APIView
from . import auth_serializers
import json
from django.contrib.auth import authenticate, login
from rest_framework import status, views
from rest_framework.response import Response
from django.contrib.auth.models import User


class RegisterAPIView(APIView):
    def post(self, request):
        serializer = auth_serializers.RegisterSerializer(data=request.data)
        if serializer.is_valid():
            try:
                user = User.objects.create_user(
                    serializer.validated_data['username'],
                    serializer.validated_data['email'],
                    serializer.validated_data['password']
                    )
                user.save()
                return Response(serializer.validated_data, status=status.HTTP_201_CREATED)
            except Exception as e:
                return Response({str(e)}, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)


class LoginView(views.APIView):
    def post(self, request, format=None):
        data = json.loads(request.body)

        username = data.get('username', None)
        password = data.get('password', None)

        user = authenticate(username=username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)

                serialized = auth_serializers.UserSerializer(user)

                return Response(serialized.data)
            else:
                return Response({
                    'error': 'Awkward! Your account has been disabled.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'error': 'Looks like your username or password is wrong. :('
            }, status=status.HTTP_400_BAD_REQUEST)


class SignUpFormView(FormView):
    form_class = UserCreationForm
    template_name = 'authorization/sign_up.html'
    success_url = '/user_auth/log_in/'

    def form_valid(self, form):
        form.save()
        return super(SignUpFormView, self).form_valid(form)


class LoginFormView(FormView):
    form_class = AuthenticationForm
    template_name = 'authorization/log_in.html'
    success_url = '/'

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super(LoginFormView, self).form_valid(form)


class LogOutView(View):

    def get(self, request, *args, **kwargs):
        logout(self.request)
        return redirect('/')
