from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^API_sign_up/$', views.RegisterAPIView.as_view(), name='register'),
    url(r'^API_login/$', views.LoginView.as_view(), name='login'),
    url(r'^sign_up/$', views.SignUpFormView.as_view(), name='sign_up'),
    url(r'^log_in/$', views.LoginFormView.as_view(), name='log_in'),
    url(r'^log_out/$', views.LogOutView.as_view(), name='log_out'),
]