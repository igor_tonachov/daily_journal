from django.views.generic.base import TemplateView


class WelcomeScreenView(TemplateView):
    template_name = 'welcome_screen.html'


class IndexView(TemplateView):
    template_name = 'index.html'
